import cv2
import os
import copy
import numpy as np
from matplotlib import pyplot as plt


class MedianFilter:
    curr_image = []
    copy_curr_image = []
    critical_value_white = [0.9]
    critical_value_black = [4]

    def __init__(self, param_file):
        self.curr_image = cv2.imread(param_file)
        self.copy_curr_image = copy.copy(self.curr_image)

    def fix_noisy_parts(self):
        rows = len(self.curr_image)
        cols = len(self.curr_image[0])
        print("Size", rows, cols)
        noise_points = 0
        for i in range(1, rows - 1):
            for j in range(1, cols - 1):
                boundary_red = [self.curr_image[i - 1, j - 1, 0], self.curr_image[i - 1, j, 0],
                                self.curr_image[i - 1, j + 1, 0],
                                self.curr_image[i, j - 1, 0], self.curr_image[i, j + 1, 0],
                                self.curr_image[i + 1, j - 1, 0], self.curr_image[i + 1, j, 0],
                                self.curr_image[i + 1, j + 1, 0]]
                boundary_red = np.array(boundary_red)
                median_red = np.median(boundary_red)

                boundary_green = [self.curr_image[i - 1, j - 1, 1], self.curr_image[i - 1, j, 1],
                                  self.curr_image[i - 1, j + 1, 1],
                                  self.curr_image[i, j - 1, 1], self.curr_image[i, j + 1, 1],
                                  self.curr_image[i + 1, j - 1, 1], self.curr_image[i + 1, j, 1],
                                  self.curr_image[i + 1, j + 1, 1]]
                boundary_green = np.array(boundary_green)
                median_green = np.median(boundary_green)

                boundary_blue = [self.curr_image[i - 1, j - 1, 2], self.curr_image[i - 1, j, 2],
                                 self.curr_image[i - 1, j + 1, 2],
                                 self.curr_image[i, j - 1, 2], self.curr_image[i, j + 1, 2],
                                 self.curr_image[i + 1, j - 1, 2], self.curr_image[i + 1, j, 2],
                                 self.curr_image[i + 1, j + 1, 2]]
                boundary_blue = np.array(boundary_blue)
                median_blue = np.median(boundary_blue)
                # Assuming a Critical Value of 1.8 I choose a value for existence of noise in an image
                try:
                    if median_red / self.curr_image[i, j, 0] > 4 and median_green / self.curr_image[i, j, 1] > 4 \
                            and median_blue / self.curr_image[i, j, 2] > 4:
                        print ("Onto Comparision")
                        noise_points += 1
                        print([median_red, median_green, median_blue])
                        print(self.curr_image[i, j])
                        self.copy_curr_image[i, j] = [median_red, median_green, median_blue]
                    if median_red / self.curr_image[i, j, 0] < 0.9 and median_green / self.curr_image[i, j, 1] < 0.9 \
                            and median_blue / self.curr_image[i, j, 2] < 0.9:
                        print ("Onto Comparision")
                        noise_points += 1
                        print([median_red, median_green, median_blue])
                        print(self.curr_image[i, j])
                        self.copy_curr_image[i, j] = [median_red, median_green, median_blue]
                except ZeroDivisionError:
                    # print([median_red, median_green, median_blue])
                    # print(self.curr_image[i, j])
                    self.copy_curr_image[i, j] = [median_red, median_green, median_blue]

        return noise_points


def get_image_files():
    list_jpg_files = [f for f in os.listdir("./noise/") if f.endswith('.jpg') or f.endswith('.png')]
    return list_jpg_files


def plot_res(image, second_image, title):
    plt.subplot(121), plt.imshow(image), plt.title('Original')
    plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(second_image), plt.title(title)
    plt.xticks([]), plt.yticks([])
    plt.show()


all_files = get_image_files()
print(all_files)
try:
    index = int(input("Enter index of the file to use Value from 0 - {}:\n".format(len(all_files) - 1)))
    filter_image = MedianFilter("noise/" + all_files[index])
    noisy_count = filter_image.fix_noisy_parts()
    print(noisy_count)
    plot_res(filter_image.curr_image, filter_image.copy_curr_image, "Noise Removed Image using Median Filter")
except IndexError as ie:
    print(ie.message)
