import cv2
import os
import copy
import random
import numpy as np
from matplotlib import pyplot as plt


class GaussianNoise:
    pixel_values = None
    copy_pixel_values = None
    gauss = None

    def __init__(self, input_file):
        self.pixel_values = np.array(cv2.imread("./images/" + input_file))
        self.copy_pixel_values = copy.copy(self.pixel_values)

    @staticmethod
    def write_to_csv(filename, contents, line_size):
        with open(filename, mode='w') as f:
            for line in contents:
                for j in range(0, line_size - 1):
                    f.write(str(line[j]) + ",")
                f.write(str(line[line_size - 1]) + "\n")

    def generate_noise_in_clear(self, auto=False):
        rows, cols, ch = np.shape(self.pixel_values)
        if not auto:
            mean = float(input("Enter mean of the Gaussian Distribution\n"))
            var = float(input("Enter variance of the Gaussian Distribution\n"))
        else:
            mean = random.uniform(6, 25)
            var = random.random() * 5
        self.gauss = np.random.normal(mean, var, (rows, cols, ch))
        self.gauss = self.gauss.reshape(rows, cols, ch)
        noisy_image = self.copy_pixel_values + self.gauss
        return mean, var, noisy_image

    def generate_actual_image(self, mean, var, noisy_img, auto):
        rows, cols, ch = np.shape(self.pixel_values)
        approx_img = np.zeros(shape=(rows, cols, ch), dtype=np.uint8)
        gauss = np.random.normal(mean, var, (rows, cols, ch))
        gauss = gauss.reshape(rows, cols, ch)
        perfect_match = 0
        for i in range(0, rows):
            for j in range(0, cols):
                match = False
                for k in range(0, 3):
                    approx_img[i, j, k] = noisy_img[i, j, k] - gauss[i, j, k]
                    if approx_img[i, j, k] == self.pixel_values[i, j, k]:
                        match = True
                if match:
                    perfect_match += 1
        if not auto:
            return approx_img
        else:
            return perfect_match


if __name__ == "__main__":
    def get_image_files():
        list_jpg_files = [f for f in os.listdir("./images") if f.endswith('.jpg') or f.endswith('.png')]
        return list_jpg_files


    def plot_res(image, second_image, title, third_image, title_sec):
        plt.subplot(221), plt.imshow(image), plt.title('Original')
        plt.xticks([]), plt.yticks([])
        plt.subplot(222), plt.imshow(second_image), plt.title(title)
        plt.xticks([]), plt.yticks([])
        plt.subplot(223), plt.imshow(third_image), plt.title(title_sec)
        plt.xticks([]), plt.yticks([])
        plt.show()

    def plot_graph_to_png_file(x_axis, y_axis, fn, label_x, label_y, range_values):
        plt.plot(x_axis, y_axis, "ro")
        plt.axis(range_values)
        plt.xlabel(label_x)
        plt.ylabel(label_y)
        file_name = "Docs/graphs/" + fn + ".png"
        plt.savefig(file_name)

    all_files = get_image_files()
    print(all_files)
    index = int(input("Enter index of the file to use Value from 0 - {}:\n".format(len(all_files) - 1)))
    automate = bool(input("Do you want to automate the results? (True/False)\n"))
    try:
        gaussian = GaussianNoise(all_files[index])
        image_rows, image_cols, rgb_values = np.shape(gaussian.pixel_values)
        image_size = image_rows * image_cols
        if not automate:
            g_mean, g_var, noisy = gaussian.generate_noise_in_clear()
            # Generate Real From Noise
            print(automate)
            approx_image = gaussian.generate_actual_image(g_mean, g_var, noisy, automate)
            plot_res(gaussian.pixel_values, noisy, "Generate Noise", approx_image, "Generate Real Image "
                                                                                            "from Noise")
        else:
            all_data = [["Mean", "Variance", "Dispersion", "Perfect Matches", "Accuracy"]]
            for it in range(0, 25):
                g_mean, g_var, noisy = gaussian.generate_noise_in_clear(automate)
                # Generate Real From Noise
                matches = gaussian.generate_actual_image(g_mean, g_var, noisy, automate)
                print(matches)
                all_data.append([g_mean, g_var, g_var/g_mean, matches, float(float(matches) / float(image_size))])
            all_data = np.array(all_data)
            plot_graph_to_png_file(all_data[1:, 1], all_data[1:, 4:], all_files[index] + "accuracy_vs_var", "Variance",
                                   "Accuracy", [0, 5, 0, 1])
            plot_graph_to_png_file(all_data[1:, 0], all_data[1:, 4:], all_files[index] + "accuracy_vs_mean", "Mean",
                                   "Accuracy", [6, 25, 0, 1])
            plot_graph_to_png_file(all_data[1:, 2], all_data[1:, 4:], all_files[index] + "accuracy_vs_dispersion",
                                   "Dispersion", "Accuracy", [0, 25, 0, 1])
            GaussianNoise.write_to_csv("Docs/" + all_files[index] + "_accuracy_noise_removal.csv", all_data, 5)
    except IndexError as ie:
        print(ie.message)
    gaussian = None
