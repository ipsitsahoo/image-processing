import cv2
import copy
import os
from matplotlib import pyplot as plt


def get_image_files():
    list_jpg_files = [f for f in os.listdir(".") if f.endswith('.jpg') or f.endswith('.png')]
    return list_jpg_files


def plot_res(image, second_image, title):
    plt.subplot(121), plt.imshow(image), plt.title('Original')
    plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(second_image), plt.title(title)
    plt.xticks([]), plt.yticks([])
    plt.show()


# Average Gray Scaling Method
def avg_color_to_gray_scale(image):
    new_img = copy.copy(image)
    for j in range(0, len(image)):
        for k in range(0, len(image[j])):
            avg_val = int(sum(image[j][k]) / len(image[j][k]))
            for l in range(0, len(image[j][k])):
                new_img[j][k][l] = avg_val
    return new_img


# Luminous Gray Scaling Method
def lum_color_to_gray_scale(image):
    new_img = copy.copy(image)
    for j in range(0, len(image)):
        for k in range(0, len(image[j])):
            avg_val = int(0.3 * image[j][k][0] + 0.59 * image[j][k][1] + 0.11 * image[j][k][2])
            for l in range(0, len(image[j][k])):
                new_img[j][k][l] = avg_val
    return new_img

if __name__ == "__main__":
    files = get_image_files()
    print(files)
    res = int(input("Enter image File Index - 0 based index\n"))
    img = cv2.imread(files[res])
    # Average Method of Blurring an Image.
    print ("1. Grays_Scale Using Average Color\n2. Gray Scale Using Weighted Method")
    case = int(input())
    new_image = []
    if case == 1:
        new_image = avg_color_to_gray_scale(img)
        plot_res(img, new_image, "Average Method")
    elif case == 2:
        new_image = lum_color_to_gray_scale(img)
        plot_res(img, new_image, "Luminous or Weighted Method")
