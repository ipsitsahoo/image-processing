import cv2
import os
from matplotlib import pyplot as plt


def get_image_files():
    list_jpg_files = [f for f in os.listdir("./images") if f.endswith('.jpg') or f.endswith('.png')]
    return list_jpg_files
all_files = get_image_files()
print(all_files)
index = int(input("Enter index of the file to use Value from 0 - {}:\n".format(len(all_files) - 1)))
img = cv2.imread("./images/" + all_files[index], 0)
first_arg = int(input("Enter the First Argument\n"))
second_arg = int(input("Enter the Second Argument\n"))
edges = cv2.Canny(img, first_arg, second_arg)

plt.subplot(121), plt.imshow(img, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122), plt.imshow(edges, cmap='gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()
