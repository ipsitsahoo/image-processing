### USING OPENCV FOR IMAGE PROCESSING

This repository contains codes regarding application of image processing using OpenCV. The repo is basically for introductory understanding to image Processing.

The following basic functions have been implemented here till now
#### IMPLEMENTATION OF IMAGE BLURRING TECHNIQUES

    1. Gray-Scale Imaging using Average and Weight/Luminous method
    2. Average Blur method for Blurring Images
    3. Gaussian Blur
    4. Median Blur

#### NOISE DETECTION IN IMAGES

- Using Median Filter for Salt and Pepper Noise in Image
    - Penguin 
    ![Median Filter Example](Docs/medianFilter_1.png)
    - Lena Soderberg
    ![Median Filter Example](Docs/medianFilter_2.png)
    - World War Fighter Plane
    ![Median Filter Example](Docs/medianFilter_3.png)
- Using Gaussian Noise to add noise to image and get the real image back
    - Mean = 10 Variance = 0.9 
    ![Median Filter Example](Docs/gaussianNoise_1.png)
    - Mean = 1 Variance = 0.1
    ![Median Filter Example](Docs/gaussianNoise_2.png)
    - Mean = 100 Variance = 25
    ![Median Filter Example](Docs/gaussianNoise_3.png)
    - Mean = 25 Variance = 2.5
    ![Median Filter Example](Docs/gaussianNoise_4.png)
- Graphs for the Results
    - IMAGE NAME: cluster.png 
    ![Accuracy vs Variance](Docs/graphs/cluster.pngaccuracy_vs_var.png)
    - IMAGE NAME: Messi.png 
    ![Accuracy vs Variance](Docs/graphs/messi-leo.jpgaccuracy_vs_var.png)   

#### DETECT EDGES IN AN IMAGE
  - Canny Edge Detection Algorithm to detect edges in an Image
    - Messi Black and White 
    ![Edge Detection Example](Docs/edge_detect_1.png) 

