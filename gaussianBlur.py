import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('image.jpg')
# Gaussian Filter to Blurring Image
kernel = np.ones((10, 10), np.float32)/100
blur = cv2.GaussianBlur(img, (5, 5), 0)
plt.subplot(121), plt.imshow(img), plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122), plt.imshow(blur), plt.title('Gaussian Blurring')
plt.xticks([]), plt.yticks([])
plt.show()
